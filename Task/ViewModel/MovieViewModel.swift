//
//  MovieViewModel.swift
//  Task
//
//  Created by Haris on 11/11/2023.
//

import Foundation

protocol MovieProtocol {
    func getPopularMovies(page: Int)
}

class MovieViewModel {
    var movieListFetched: (([Results]) -> ())?
    var movieDetailFetched: ((MovieDetailModel) -> ())?
}

extension MovieViewModel: MovieProtocol {
    func getPopularMovies(page: Int) {
        ApiManager.shared.getServiceData(url: "https://api.themoviedb.org/3/movie/popular?language=en-US&page=\(page)", method: .get) { (response: MoviesListModel!, error: String?) in
            if let error = error {
                debugPrint(error)
                return
            }
            self.movieListFetched?(response.results ?? [])
        }
    }
    func getMovieDetail(movieID: Int) {
        ApiManager.shared.getServiceData(url: "https://api.themoviedb.org/3/movie/\(movieID)", method: .get) { (response: MovieDetailModel!, error: String?) in
            if let error = error {
                debugPrint(error)
                return
            }
            print(response)
            self.movieDetailFetched?(response)
        }
    }
    
}
