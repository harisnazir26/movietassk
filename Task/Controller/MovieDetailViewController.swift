//
//  MovieDetailViewController.swift
//  Task
//
//  Created by Haris on 11/11/2023.
//

import UIKit

class MovieDetailViewController: UIViewController {
    
    private let backView = View(backgroundColor: .clear)
    private let titleLabel: Label = Label(text: "",
                                          font: .systemFont(ofSize: 28, weight: .bold),
                                          color: .black,
                                          alignment: .left)
    private let detailLabel: Label = Label(text: "",
                                           font: .systemFont(ofSize: 18),
                                           color: .darkGray,
                                           alignment: .left)
    private let ratingLabel: Label = Label(text: "",
                                           font: .systemFont(ofSize: 18, weight: .semibold),
                                           color: .red,
                                           alignment: .right)
    private let ratingTitleLabel: Label = Label(text: "Rating:",
                                           font: .systemFont(ofSize: 18, weight: .semibold),
                                           color: .black,
                                           alignment: .right)
    private let posterImageView = ImageView(imageName: "",
                                            cornerRadius: 5,
                                            backgroundColor: .systemGray)

    private lazy var viewModel = MovieViewModel()
    
    init(movieID: Int) {
        super.init(nibName: nil, bundle: nil)
        bindViews()
        viewModel.getMovieDetail(movieID: movieID)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
    }
    private func bindViews() {
        viewModel.movieDetailFetched = { [weak self] detail in
            guard let self = self else {
                return
            }
            self.setupUI(detail: detail)
        }
    }
    private func setupUI(detail: MovieDetailModel) {
        self.posterImageView.LoadImage(detail.backdrop_path ?? "")
        self.titleLabel.text = detail.title
        self.detailLabel.text = detail.overview
        self.ratingLabel.text = "\(detail.vote_average ?? 0.0)"
    }
    private func setupViews() {
        view.addSubview(backView)
        backView.addSubview(posterImageView)
        backView.addSubview(titleLabel)
        backView.addSubview(detailLabel)
        backView.addSubview(ratingTitleLabel)
        backView.addSubview(ratingLabel)
        
        NSLayoutConstraint.activate([
            backView.topAnchor.constraint(equalTo: self.view.topAnchor),
            backView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            backView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            backView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),

            posterImageView.leadingAnchor.constraint(equalTo: backView.leadingAnchor),
            posterImageView.trailingAnchor.constraint(equalTo: backView.trailingAnchor),
            posterImageView.topAnchor.constraint(equalTo: backView.topAnchor),
            posterImageView.heightAnchor.constraint(equalToConstant: 250),
            
            titleLabel.topAnchor.constraint(equalTo: posterImageView.bottomAnchor, constant: 22),
            titleLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor,constant: 10),
            titleLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: -10),
            
            ratingTitleLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            ratingTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            
            ratingLabel.leadingAnchor.constraint(equalTo: ratingTitleLabel.trailingAnchor, constant: 10),
            ratingLabel.centerYAnchor.constraint(equalTo: ratingTitleLabel.centerYAnchor),
            
            detailLabel.topAnchor.constraint(equalTo: ratingTitleLabel.bottomAnchor, constant: 20),
            detailLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            detailLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            
        ])
    }
    


}
