//
//  ViewController.swift
//  Task
//
//  Created by Haris on 09/11/2023.
//

import UIKit

class ViewController: UIViewController {
    
    private lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.separatorStyle = .none
        tv.allowsMultipleSelectionDuringEditing = true
        tv.allowsMultipleSelection = true
        tv.allowsSelection = true
        tv.allowsMultipleSelectionDuringEditing = true
        tv.estimatedRowHeight = 164
        tv.backgroundColor = .white
        tv.dataSource = self
        tv.delegate = self
        tv.showsVerticalScrollIndicator = false
        tv.register(MovieTableViewCell.self, forCellReuseIdentifier: MovieTableViewCell.identifier)
        return tv
    }()
    private lazy var list = [Results]()
    private lazy var viewModel = MovieViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        self.title = "Popular Movies"
        setupViews()
        bindViews()
        viewModel.getPopularMovies(page: 1)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    private func bindViews() {
        viewModel.movieListFetched = { [weak self] list in
            guard let self = self else {
                return
            }
            self.list = list
            self.tableView.reloadData()
        }
    }
    
    private func setupViews() {
       
        self.view.addSubview(tableView)
      
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
        
    }
    
    private func moveToDetailController(_ movieID: Int) {
        self.navigationController?.pushViewController(MovieDetailViewController(movieID: movieID), animated: true)
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieTableViewCell.identifier) as! MovieTableViewCell
        cell.data = list[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        moveToDetailController(list[indexPath.row].id ?? 0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}
