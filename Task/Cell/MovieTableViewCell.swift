//
//  MovieTableViewCell.swift
//  Task
//
//  Created by Haris on 11/11/2023.
//

import UIKit

class MovieTableViewCell: UITableViewCell{
    
    static let identifier = "MovieTableViewCell"
    var data: Results! {
        didSet{
            titleLabel.text = data.original_title
            detailLabel.text = data.overview
            posterImageView.LoadImage(data.backdrop_path ?? "")
        }
    }
    
    private let backView: View = View(backgroundColor: .systemGray5,
                              cornerRadius: 18)
    private let titleLabel: Label = Label(text: "---",
                                  font: .systemFont(ofSize: 20, weight: .bold),
                                  color: .black,
                                  alignment: .left)
    private let detailLabel: Label = Label(text: "---",
                                   font: .systemFont(ofSize: 14),
                                   color: .darkGray,
                                   alignment: .left,
                                   numberOfLines: 2)
    private let posterImageView = ImageView(imageName: "",
                                    cornerRadius: 5,
                                    backgroundColor: .systemGray)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .white
        self.selectionStyle = .none
        self.contentView.isUserInteractionEnabled = true
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(backView)
        backView.addSubview(posterImageView)
        backView.addSubview(titleLabel)
        backView.addSubview(detailLabel)
        
        NSLayoutConstraint.activate([
            backView.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            backView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5),
            backView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15),
            backView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15),

            posterImageView.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: 22),
            posterImageView.centerYAnchor.constraint(equalTo: backView.centerYAnchor),
            posterImageView.heightAnchor.constraint(equalToConstant: 80),
            posterImageView.widthAnchor.constraint(equalToConstant: 80),
            
            titleLabel.topAnchor.constraint(equalTo: backView.topAnchor, constant: 22),
            titleLabel.leadingAnchor.constraint(equalTo: posterImageView.trailingAnchor,constant: 15),
            titleLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: -22),
            
            detailLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8),
            detailLabel.bottomAnchor.constraint(equalTo: backView.bottomAnchor, constant: -22),
            detailLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            detailLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            
        ])
    }
}



