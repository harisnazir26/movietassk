//
//  Label.swift
//  Task
//
//  Created by Haris on 10/11/2023.
//

import UIKit
class Label: UILabel {
    required init(text: String = "", font: UIFont = UIFont.boldSystemFont(ofSize: 12), color: UIColor = UIColor.black, alignment: NSTextAlignment = .center, numberOfLines: Int = 0) {
        super.init(frame: .zero)
        super.translatesAutoresizingMaskIntoConstraints = false
        self.semanticContentAttribute = .forceLeftToRight
        textColor = color
        self.text = text
        self.font = UIFont(name: (font.fontName), size: CGFloat(Int(font.pointSize)))
        self.textAlignment = alignment
        self.numberOfLines = numberOfLines
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
