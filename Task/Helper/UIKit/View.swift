//
//  View.swift
//  Task
//
//  Created by Haris on 10/11/2023.
//

import UIKit
class View: UIView {
    required init(backgroundColor: UIColor = UIColor.white, cornerRadius: CGFloat = 0) {
        super.init(frame: .zero)
        super.translatesAutoresizingMaskIntoConstraints = false
        
        self.backgroundColor = backgroundColor
        self.layer.cornerRadius = cornerRadius
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
