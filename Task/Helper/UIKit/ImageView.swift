//
//  ImageView.swift
//  Task
//
//  Created by Haris on 10/11/2023.
//

import UIKit
class ImageView: UIImageView {
    
    required init(imageName: String = "",cornerRadius: CGFloat = 0.0, backgroundColor: UIColor = .clear) {
        super.init(frame: .zero)
        super.translatesAutoresizingMaskIntoConstraints = false
        
        image = UIImage(named: imageName)
        self.backgroundColor = backgroundColor
        self.layer.cornerRadius = cornerRadius
        self.contentMode = .scaleToFill
        self.layer.masksToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
