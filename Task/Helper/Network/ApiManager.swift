//
//  ApiManager.swift
//  Task
//
//  Created by Haris on 09/11/2023.
//

import Foundation
import Alamofire

class ApiManager {
    static let shared = ApiManager()
    private init() {}
    
    func getServiceData <T: Decodable> (url: String, method: HTTPMethod, parameters: [String:Any]? = nil, completion: @escaping (T?, String?) ->()) {
        
        let headerDic = [
          "accept": "application/json",
          "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxOTg4ZTk5ZDI2OWI3NjJlNGQxNDJmYmYzNGZjYjJkNyIsInN1YiI6IjY1NGNjZDFiNDFhNTYxMzM2ZDg2ZGM0ZSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.O5OTerCU40ct4oextP7u-zIVIufNmTcXPgccJIVt0M4"
        ]
        let headers = HTTPHeaders(headerDic)
//        HTTPHeader(name: "Authorization", value: "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxOTg4ZTk5ZDI2OWI3NjJlNGQxNDJmYmYzNGZjYjJkNyIsInN1YiI6IjY1NGNjZDFiNDFhNTYxMzM2ZDg2ZGM0ZSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.O5OTerCU40ct4oextP7u-zIVIufNmTcXPgccJIVt0M4")
        AF.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            if response.error != nil {
                debugPrint(response.error?.localizedDescription ?? "")
                completion(nil, response.error?.localizedDescription)
            }
            else {
                guard let data = response.data else {
                    return completion(nil, response.error?.localizedDescription)
                }
                do{
                    let returnedResponse = try JSONDecoder().decode(T.self, from: data)
                    completion(returnedResponse, nil)
                } catch {
                    debugPrint(error)
                    completion(nil, error.localizedDescription)
                }
                
            }
            
        }
    }
    
}
