//
//  ImageViewExtension.swift
//  Task
//
//  Created by Haris on 11/11/2023.
//

import UIKit
import Kingfisher

extension UIImageView {
    
    func LoadImage(_ name: String) {
        self.kf.indicatorType = .activity
        self.kf.setImage(with: URL(string: "https://image.tmdb.org/t/p/original\(name)")!)
    }
}
